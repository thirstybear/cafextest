package uk.co.thirstybear.cafex

case object Menu {
  val items = List(
    Drink("Cola", 0.5),
    Drink("Coffee", 1.0),
    ColdFood("Cheese Sandwich", 2.0),
    HotFood("Steak Sandwich", 4.5)
  )
}

trait Item {
  def name:String
  def price:Double
  def serviceCharge:Double = 0.0
}

trait ServiceCharge10 extends Item {
  override def serviceCharge = 0.1
}

trait ServiceCharge20 extends Item {
  override def serviceCharge = 0.2
}

case class Drink(name: String, price:Double) extends Item
case class ColdFood(name: String, price:Double) extends Item with ServiceCharge10
case class HotFood(name: String, price:Double) extends Item with ServiceCharge20