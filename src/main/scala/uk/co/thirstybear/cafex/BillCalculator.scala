package uk.co.thirstybear.cafex

import scala.math.BigDecimal.RoundingMode.HALF_UP

case class BillCalculator(menuItems: List[Item]) {

  def total(items: List[String]): Double = {
      val billedItems = items.flatMap(findItem)
      val subTotal = billedItems.map(_.price).sum
      val serviceChargeFactor = billedItems.map(_.serviceCharge).foldLeft(0.0)(_ max _)
      val serviceCharge = (serviceChargeFactor * subTotal).min(20.0)

      BigDecimal(subTotal + serviceCharge).setScale(2, HALF_UP).toDouble
  }

  def findItem(name: String): Option[Item] = {
    menuItems.find(name == _.name)
  }
}

