package uk.co.thirstybear.cafex

import org.scalatest.{FlatSpec, Matchers}

class BillCalculatorSpec extends FlatSpec with Matchers {
  case class TestItem(name: String, price:Double) extends Item with ServiceCharge10

  val billCalculator = BillCalculator(TestItem("Round Down Salad", 1.01) :: TestItem("Round Up Cake", 1.06) :: Menu.items )

  "A Bill" should "have value of zero when empty" in {
    billCalculator.total(List.empty) should be(0.0)
  }

  it should "ignore unrecognised menu items" in {
    billCalculator.total(List("Wombat Sandwich")) should be (0.0)
    billCalculator.total(List("Cola", "Coffee", "Wombat Sandwich")) should be (1.5)
  }

  it should "not include a service charge if it is drinks only" in {
    billCalculator.total(List("Cola", "Cola", "Coffee", "Coffee")) should be(3.00)
  }

  it should "add a 10% service charge if it includes cold food" in {
    billCalculator.total(List("Cola", "Coffee", "Cheese Sandwich")) should be(3.85)
  }

  it should "add a 20% service charge if it includes hot food" in {
    billCalculator.total(List("Cola", "Coffee", "Steak Sandwich")) should be(7.20)
  }

  it should "round to 2dp" in {
    billCalculator.total(List("Round Down Salad")) should be (1.11)
    billCalculator.total(List("Round Up Cake")) should be (1.17)
  }

  it should "limit any service charge to £20" in {
    billCalculator.total(List.fill(25)("Steak Sandwich")) should be(132.50)
  }
}