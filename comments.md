# Some thoughts

## With the existing menu, it is impossible to test 2 decimal place rounding
There is no way to show that the 2dp requirement is met using the existing menu items. Also, there is the conflicting 
requirement "_At each step build the simplest possible solution to meet the requirements_".

So there is a very strong argument for _not_ adding any rounding code to the existing codebase. The edge case can never 
be triggered. YAGNI!

However, is it reasonable to have a fixed, hard-coded menu in real life? I suspect not. So in the absence of customer 
feedback (a depressingly common failure mode in software projects in general), the ability to change the menu itself
has been added, and used to drive out the issue with possible 2 decimal place problems.

## Ambiguity with the maximum service charge
Reading the specification, there is a £20 maximum service charge. However, the way it is worded, it is unclear if the 
maximum only applies to bills that include hot food, or if it applies to _all_ orders of any type with a service charge. 

This is a fundamental shortfall of written specification - the English language can be imprecise, even when care is 
taken (look up the classic developer joke "_"Please go to the store and buy a carton of milk and if they have eggs, get 
six._". Again, a depressingly common failure of software projects. 

Once again the absence of customer feedback ensures clarification is not possible, so the service charge cap has been 
applied across all bills. 